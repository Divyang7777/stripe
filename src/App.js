import React from 'react';
import logo from './logo.svg';
import './App.css';
import * as stripe from 'stripe'
import StripeCheckout from 'react-stripe-checkout';
import * as uuid from 'uuid'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.stripe = require('stripe')('sk_test_o7VYrK9Fyqd8LhuuUuNAlTGX');
    debugger
    // this.getDetails()
    // this.getPaymentIntent()
  }

  // async getDetails() {
  //   debugger
  //   const details = await this.stripe.charges.retrieve('ch_1BN2szIsJJYnri3UKbisDJNp', {
  //     api_key: 'sk_test_o7VYrK9Fyqd8LhuuUuNAlTGX'
  //   });
  //   debugger
  // }

  // async getPaymentIntent() {
  //   // ca_GxguKQB6CORZYmxgwbJu3ZpvKWevXRZP
  //   debugger
  //   const paymentIntent = await this.stripe.paymentIntents.create({
  //     payment_method_types: ['card'],
  //     amount: 1000,
  //     currency: 'inr',
  //   }, {
  //     stripeAccount: 'ca_GxguKQB6CORZYmxgwbJu3ZpvKWevXRZP',
  //   }).then(function (paymentIntent) {
  //     // asynchronously called
  //     debugger
  //   });
  //   debugger
  // }

  async onToken(token, that) {
    debugger
    const customer = await that.stripe.customers.create({
      email: token.email,
      source: token.id
    });
    debugger
    const idempotency_key = uuid();
    const charge = await that.stripe.charges.create(
      {
        amount: 100,
        currency: "usd",
        customer: customer.id,
        receipt_email: token.email,
        description: "From the Demo App !!!",
        shipping: {
          name: token.card.name,
          address: {
            line1: "",
            line2: "",
            city: "",
            country: "",
            postal_code: ""
          }
        }
      },
      {
        idempotency_key
      }
    );
    debugger
  }

  render() {
    return (
      <div className="App" >
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          <StripeCheckout
            token={(val) => this.onToken(val, this)}
            stripeKey="pk_test_JSmNGUWGm5iChHzfYwqy0Ygy"
            label="Pay"
            zipCode={true}
          />
          {/* ca_GxguKQB6CORZYmxgwbJu3ZpvKWevXRZP */}
          {/* <a target="_blank" className="App-link"
            href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_GxguKQB6CORZYmxgwbJu3ZpvKWevXRZP&scope=read_write">
            Click here to link to stripe </a> */}
        </header>
      </div>
    );
  }
}

export default App;
